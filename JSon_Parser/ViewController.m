//
//  ViewController.m
//  JSon_Parser
//
//  Created by Bruno Tavares on 03/02/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
#define kLatestKivaLoansURL [NSURL URLWithString:@"http://api.kivaws.org/v1/loans/search.json?status=fundraising"] //2

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *parsedText;
@property (strong, nonatomic) IBOutlet UILabel *summary;
@property (strong, nonatomic) IBOutlet UIView *loadingView;

@end

@implementation ViewController

// set status bar to white
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.loadingView.hidden = false;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: kLatestKivaLoansURL];
        
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    });
    
    
}

- (void)fetchedData:(NSData *)responseData {
    
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSArray* latestLoans = [json objectForKey:@"loans"]; //2
    
    NSLog(@"loans: %@", latestLoans); //3
    
    // get the latest loan
    NSDictionary* loan = [latestLoans objectAtIndex:0];
    
    // get the funded amount and loan amount
    NSNumber *fundedAmount = [loan objectForKey:@"funded_amount"];
    NSNumber *loanAmount = [loan objectForKey:@"loan_amount"];
    float outstandingAmount = [loanAmount floatValue] - [fundedAmount floatValue];
    
    // set the labels
    self.parsedText.text = [NSString stringWithFormat: @"Latest loan: %@ from %@ needs another $%.2f to pursue their entreprenueral dream",
                            [loan objectForKey:@"name"],
                            [(NSDictionary*)[loan objectForKey:@"location"] objectForKey:@"country"],
                            outstandingAmount];
    
    //build an info object
    NSDictionary* info = [NSDictionary dictionaryWithObjectsAndKeys:
                          [loan objectForKey:@"name"],
                          @"who",
                          [(NSDictionary*)[loan objectForKey:@"location"]
                           objectForKey:@"country"],
                          @"where",
                          [NSNumber numberWithFloat: outstandingAmount],
                          @"what",
                          nil];
    
    //convert the object into JSON
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:info 
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    self.summary.text = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //hide the loading view
    self.loadingView.hidden = true;
}
@end
